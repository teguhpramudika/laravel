@extends('layouts.app')

@section('title','Detail Travel')


@section('content')
<main>

    <section class="section-details-header"></section>
    <section class="section-details-content">

        <div class="container">
            <div class="row">
                <div class="col p-0">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item">

                                Paket Travel
                            </li>

                            <li class="breadcrumb-item active">

                                Details
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 pl-lg-0">
                    <div class="card card-details">
                        <h1>Nusa penida</h1>
                        <p>
                            Republicof Indonesia Raya
                        </p>
                        <div class="gallery">
                            <div class="xzoom-container">
                                <img src="frontend/images/detail.jpg" class="xzoom" id="xzoom-default"
                                    xoriginal="frontend/images/detail.jpg">
                            </div>
                            <div class="xzoom-thumbs">
                                <a href="frontend/images/detail.jpg">
                                    <img src="frontend/images/detail.jpg" class="xzoom-gallery" width="128"
                                        xpreview="frontend/images/detail.jpg">
                                </a>
                                <a href="frontend/images/detail2.jpg">
                                    <img src="frontend/images/detail2.jpg" class="xzoom-gallery" width="128"
                                        xpreview="frontend/images/detail2.jpg">
                                </a>
                                <a href="frontend/images/detail3.jpg">
                                    <img src="frontend/images/detail3.jpg" class="xzoom-gallery" width="128"
                                        xpreview="frontend/images/detail3.jpg">
                                </a>
                                <a href="frontend/images/detail4.jpg">
                                    <img src="frontend/images/detail4.jpg" class="xzoom-gallery" width="128"
                                        xpreview="frontend/images/detail4.jpg">
                                </a>
                                <a href="frontend/images/pantai.jpg">
                                    <img src="frontend/images/pantai.jpg" class="xzoom-gallery" width="128"
                                        xpreview="frontend/images/pantai.jpg">
                                </a>
                            </div>
                        </div>
                        <h2>Tentang Wisata</h2>
                        <p>
                            Nusa Penida is an island southeast of Indonesia’s island Bali and a district of
                            Klungkung
                            Regency that includes the neighbouring small island of Nusa Lembongan. The Badung
                            Strait separates the island and Bali. The interior of Nusa Penida is hilly with a
                            maximum
                            altitude of 524 metres. It is drier than the nearby island of Bali.
                        </p>
                        <p>
                            Bali and a district of Klungkung Regency that includes the neighbouring small island of
                            Nusa Lembongan. The Badung Strait separates the island and Bali.
                        </p>
                        <div class="features row">
                            <div class="col-md-4 border-left ">
                                <img src="frontend/images/ic_event@2x.png" alt="" class="features-images">
                                <div class="description">

                                    <h3>

                                        Featured Event
                                    </h3>

                                    <p>Tari kecak</p>
                                </div>
                            </div>

                            <div class="col-md-4 border-left">
                                <img src="frontend/images/ic_bahasa.png" alt="" class="features-images">

                                <div class="description">


                                    <div class="description">

                                        <h3>

                                            Languange
                                        </h3>

                                        <p>Bahasa Indonesia</p>

                                    </div>


                                </div>
                            </div>
                            <div class="col-md-4 border-left">
                                <img src="frontend/images/ic_foods.png" alt="" class="features-images">
                                <div class="description">

                                    <h3>

                                        Foods
                                    </h3>

                                    <p>Local Foods</p>



                                </div>



                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-4">

                    <div class="card card-details card-right">

                        <h2>Members are going</h2>
                        <div class="members my-2">

                    <img src="frontend/images/shayna3.jpg" 
                    class="member-image mr-1" alt="">
                    <img src="frontend/images/lala.jpg" 
                    class="member-image mr-1" alt="">
                    <img src="frontend/images/tejo.jpg" 
                    class="member-image mr-1" alt="">
                    <img src="frontend/images/sutejo.png" 
                    class="member-image mr-1" alt="">  <img src="frontend/images/shayna3.jpg" 
                    class="member-image mr-1" alt="">
                        </div>
                        <hr>
                        <h2>Trip informations</h2>



                        <table class="trip-informations">
                            <tr>

                                <th width="50%">Date of Departure</th>
                                <td width="50%" class=text-right>

                                    22 Aug,2019

                                </td>
                            </tr>

                            <tr>

                                <th width="50%">Duration</th>
                                <td width="50%" class=text-right>

                                    4D 3N

                                </td>
                            </tr>

                            <tr>

                                <th width="50%">Type</th>
                                <td width="50%" class=text-right>

                                    Open Trip

                                </td>
                            </tr>

                            <tr>

                                <th width="50%">Price</th>
                                <td width="50%" class=text-right>

                                $80,00 / person

                                </td>
                            </tr>

                        </table>
                    </div>

                    <div class="join-container text-center">

                    <a href="{{url('checkout')}}" class="btn btn-block btn-join-now mt-3 py-2">
                            JOIN NOW 
                            
                        
                        </a>
                    </div>


                </div>



            </div>
        </div>
        </div>
    </section>
</main>

@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{url('    <link rel="stylesheet" href="frontend/libraries/xzoom/xzoom.css">
')}}">

@endpush

@push('addon-script')
<script src="{{url('frontend/libraries/xzoom/xzoom.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.xzoom, .xzoom-gallery').xzoom({
            zoomWidth:400,
            title: true,
            tint: '#333',
            Xoffset: 15
        });

        // $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 400, title: true, tint: '#333', Xoffset: 15});
    });
</script>
@endpush
